/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author User
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeforestacionDTO {
    
@SerializedName("country")
@Expose
private String country;

@SerializedName("iso3")
@Expose
private String iso3;

@SerializedName("wdpaId")
@Expose
private String wdpaId;

@SerializedName("parkName")
@Expose
private String parkName;

@SerializedName("year")
@Expose
private String year;

@SerializedName("outside")
@Expose
private String outside;

@SerializedName("inside")
@Expose
private String inside;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public String getWdpaId() {
        return wdpaId;
    }

    public void setWdpaId(String wdpaId) {
        this.wdpaId = wdpaId;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getOutside() {
        return outside;
    }

    public void setOutside(String outside) {
        this.outside = outside;
    }

    public String getInside() {
        return inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }



}


package DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractDTO{
@SerializedName("country")
@Expose
private String country;
@SerializedName("countryCode")
@Expose
private String countryCode;
@SerializedName("indicatorName")
@Expose
private String indicatorName;
@SerializedName("indicatorCode")
@Expose
private String indicatorCode;
@SerializedName("a1960")
@Expose
private String a1960;
@SerializedName("a1961")
@Expose
private String a1961;
@SerializedName("a1962")
@Expose
private String a1962;
@SerializedName("a1963")
@Expose
private String a1963;
@SerializedName("a1964")
@Expose
private String a1964;
@SerializedName("a1965")
@Expose
private String a1965;
@SerializedName("a1966")
@Expose
private String a1966;
@SerializedName("a1967")
@Expose
private String a1967;
@SerializedName("a1968")
@Expose
private String a1968;
@SerializedName("a1969")
@Expose
private String a1969;
@SerializedName("a1970")
@Expose
private String a1970;
@SerializedName("a1971")
@Expose
private String a1971;
@SerializedName("a1972")
@Expose
private String a1972;
@SerializedName("a1973")
@Expose
private String a1973;
@SerializedName("a1974")
@Expose
private String a1974;
@SerializedName("a1975")
@Expose
private String a1975;
@SerializedName("a1976")
@Expose
private String a1976;
@SerializedName("a1977")
@Expose
private String a1977;
@SerializedName("a1978")
@Expose
private String a1978;
@SerializedName("a1979")
@Expose
private String a1979;
@SerializedName("a1980")
@Expose
private String a1980;
@SerializedName("a1981")
@Expose
private String a1981;
@SerializedName("a1982")
@Expose
private String a1982;
@SerializedName("a1983")
@Expose
private String a1983;
@SerializedName("a1984")
@Expose
private String a1984;
@SerializedName("a1985")
@Expose
private String a1985;
@SerializedName("a1986")
@Expose
private String a1986;
@SerializedName("a1987")
@Expose
private String a1987;
@SerializedName("a1988")
@Expose
private String a1988;
@SerializedName("a1989")
@Expose
private String a1989;
@SerializedName("a1990")
@Expose
private String a1990;
@SerializedName("a1991")
@Expose
private String a1991;
@SerializedName("a1992")
@Expose
private String a1992;
@SerializedName("a1993")
@Expose
private String a1993;
@SerializedName("a1994")
@Expose
private String a1994;
@SerializedName("a1995")
@Expose
private String a1995;
@SerializedName("a1996")
@Expose
private String a1996;
@SerializedName("a1997")
@Expose
private String a1997;
@SerializedName("a1998")
@Expose
private String a1998;
@SerializedName("a1999")
@Expose
private String a1999;
@SerializedName("a2000")
@Expose
private String a2000;
@SerializedName("a2001")
@Expose
private String a2001;
@SerializedName("a2002")
@Expose
private String a2002;
@SerializedName("a2003")
@Expose
private String a2003;
@SerializedName("a2004")
@Expose
private String a2004;
@SerializedName("a2005")
@Expose
private String a2005;
@SerializedName("a2006")
@Expose
private String a2006;
@SerializedName("a2007")
@Expose
private String a2007;
@SerializedName("a2008")
@Expose
private String a2008;
@SerializedName("a2009")
@Expose
private String a2009;
@SerializedName("a2010")
@Expose
private String a2010;
@SerializedName("a2011")
@Expose
private String a2011;
@SerializedName("a2012")
@Expose
private String a2012;
@SerializedName("a2013")
@Expose
private String a2013;
@SerializedName("a2014")
@Expose
private String a2014;
@SerializedName("a2015")
@Expose
private String a2015;
@SerializedName("a2016")
@Expose
private String a2016;
@SerializedName("a2017")
@Expose
private String a2017;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public String getIndicatorCode() {
        return indicatorCode;
    }

    public void setIndicatorCode(String indicatorCode) {
        this.indicatorCode = indicatorCode;
    }

    public String getA1960() {
        return a1960;
    }

    public void setA1960(String a1960) {
        this.a1960 = a1960;
    }

    public String getA1961() {
        return a1961;
    }

    public void setA1961(String a1961) {
        this.a1961 = a1961;
    }

    public String getA1962() {
        return a1962;
    }

    public void setA1962(String a1962) {
        this.a1962 = a1962;
    }

    public String getA1963() {
        return a1963;
    }

    public void setA1963(String a1963) {
        this.a1963 = a1963;
    }

    public String getA1964() {
        return a1964;
    }

    public void setA1964(String a1964) {
        this.a1964 = a1964;
    }

    public String getA1965() {
        return a1965;
    }

    public void setA1965(String a1965) {
        this.a1965 = a1965;
    }

    public String getA1966() {
        return a1966;
    }

    public void setA1966(String a1966) {
        this.a1966 = a1966;
    }

    public String getA1967() {
        return a1967;
    }

    public void setA1967(String a1967) {
        this.a1967 = a1967;
    }

    public String getA1968() {
        return a1968;
    }

    public void setA1968(String a1968) {
        this.a1968 = a1968;
    }

    public String getA1969() {
        return a1969;
    }

    public void setA1969(String a1969) {
        this.a1969 = a1969;
    }

    public String getA1970() {
        return a1970;
    }

    public void setA1970(String a1970) {
        this.a1970 = a1970;
    }

    public String getA1971() {
        return a1971;
    }

    public void setA1971(String a1971) {
        this.a1971 = a1971;
    }

    public String getA1972() {
        return a1972;
    }

    public void setA1972(String a1972) {
        this.a1972 = a1972;
    }

    public String getA1973() {
        return a1973;
    }

    public void setA1973(String a1973) {
        this.a1973 = a1973;
    }

    public String getA1974() {
        return a1974;
    }

    public void setA1974(String a1974) {
        this.a1974 = a1974;
    }

    public String getA1975() {
        return a1975;
    }

    public void setA1975(String a1975) {
        this.a1975 = a1975;
    }

    public String getA1976() {
        return a1976;
    }

    public void setA1976(String a1976) {
        this.a1976 = a1976;
    }

    public String getA1977() {
        return a1977;
    }

    public void setA1977(String a1977) {
        this.a1977 = a1977;
    }

    public String getA1978() {
        return a1978;
    }

    public void setA1978(String a1978) {
        this.a1978 = a1978;
    }

    public String getA1979() {
        return a1979;
    }

    public void setA1979(String a1979) {
        this.a1979 = a1979;
    }

    public String getA1980() {
        return a1980;
    }

    public void setA1980(String a1980) {
        this.a1980 = a1980;
    }

    public String getA1981() {
        return a1981;
    }

    public void setA1981(String a1981) {
        this.a1981 = a1981;
    }

    public String getA1982() {
        return a1982;
    }

    public void setA1982(String a1982) {
        this.a1982 = a1982;
    }

    public String getA1983() {
        return a1983;
    }

    public void setA1983(String a1983) {
        this.a1983 = a1983;
    }

    public String getA1984() {
        return a1984;
    }

    public void setA1984(String a1984) {
        this.a1984 = a1984;
    }

    public String getA1985() {
        return a1985;
    }

    public void setA1985(String a1985) {
        this.a1985 = a1985;
    }

    public String getA1986() {
        return a1986;
    }

    public void setA1986(String a1986) {
        this.a1986 = a1986;
    }

    public String getA1987() {
        return a1987;
    }

    public void setA1987(String a1987) {
        this.a1987 = a1987;
    }

    public String getA1988() {
        return a1988;
    }

    public void setA1988(String a1988) {
        this.a1988 = a1988;
    }

    public String getA1989() {
        return a1989;
    }

    public void setA1989(String a1989) {
        this.a1989 = a1989;
    }

    public String getA1990() {
        return a1990;
    }

    public void setA1990(String a1990) {
        this.a1990 = a1990;
    }

    public String getA1991() {
        return a1991;
    }

    public void setA1991(String a1991) {
        this.a1991 = a1991;
    }

    public String getA1992() {
        return a1992;
    }

    public void setA1992(String a1992) {
        this.a1992 = a1992;
    }

    public String getA1993() {
        return a1993;
    }

    public void setA1993(String a1993) {
        this.a1993 = a1993;
    }

    public String getA1994() {
        return a1994;
    }

    public void setA1994(String a1994) {
        this.a1994 = a1994;
    }

    public String getA1995() {
        return a1995;
    }

    public void setA1995(String a1995) {
        this.a1995 = a1995;
    }

    public String getA1996() {
        return a1996;
    }

    public void setA1996(String a1996) {
        this.a1996 = a1996;
    }

    public String getA1997() {
        return a1997;
    }

    public void setA1997(String a1997) {
        this.a1997 = a1997;
    }

    public String getA1998() {
        return a1998;
    }

    public void setA1998(String a1998) {
        this.a1998 = a1998;
    }

    public String getA1999() {
        return a1999;
    }

    public void setA1999(String a1999) {
        this.a1999 = a1999;
    }

    public String getA2000() {
        return a2000;
    }

    public void setA2000(String a2000) {
        this.a2000 = a2000;
    }

    public String getA2001() {
        return a2001;
    }

    public void setA2001(String a2001) {
        this.a2001 = a2001;
    }

    public String getA2002() {
        return a2002;
    }

    public void setA2002(String a2002) {
        this.a2002 = a2002;
    }

    public String getA2003() {
        return a2003;
    }

    public void setA2003(String a2003) {
        this.a2003 = a2003;
    }

    public String getA2004() {
        return a2004;
    }

    public void setA2004(String a2004) {
        this.a2004 = a2004;
    }

    public String getA2005() {
        return a2005;
    }

    public void setA2005(String a2005) {
        this.a2005 = a2005;
    }

    public String getA2006() {
        return a2006;
    }

    public void setA2006(String a2006) {
        this.a2006 = a2006;
    }

    public String getA2007() {
        return a2007;
    }

    public void setA2007(String a2007) {
        this.a2007 = a2007;
    }

    public String getA2008() {
        return a2008;
    }

    public void setA2008(String a2008) {
        this.a2008 = a2008;
    }

    public String getA2009() {
        return a2009;
    }

    public void setA2009(String a2009) {
        this.a2009 = a2009;
    }

    public String getA2010() {
        return a2010;
    }

    public void setA2010(String a2010) {
        this.a2010 = a2010;
    }

    public String getA2011() {
        return a2011;
    }

    public void setA2011(String a2011) {
        this.a2011 = a2011;
    }

    public String getA2012() {
        return a2012;
    }

    public void setA2012(String a2012) {
        this.a2012 = a2012;
    }

    public String getA2013() {
        return a2013;
    }

    public void setA2013(String a2013) {
        this.a2013 = a2013;
    }

    public String getA2014() {
        return a2014;
    }

    public void setA2014(String a2014) {
        this.a2014 = a2014;
    }

    public String getA2015() {
        return a2015;
    }

    public void setA2015(String a2015) {
        this.a2015 = a2015;
    }

    public String getA2016() {
        return a2016;
    }

    public void setA2016(String a2016) {
        this.a2016 = a2016;
    }

    public String getA2017() {
        return a2017;
    }

    public void setA2017(String a2017) {
        this.a2017 = a2017;
    }


}
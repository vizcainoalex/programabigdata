package com.mycompany.generararchivosdata;

import DTO.AbstractDTO;
import DTO.DeforestacionDTO;
import DTO.PoblacionUrbanaDTO;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;

public class NetClientGet extends AbstractDelegate{

    ObjectMapper objectMapper = new ObjectMapper();
    
    public boolean gestionarGet(String opcion,String rutaSalida, String urlRestApi){
        boolean respuesta = false;
        try{
            if(opcion.equalsIgnoreCase("Deforestacion")){
                respuesta = metodoGetDeforestacion(rutaSalida, urlRestApi);
            }else{
                respuesta = metodoGetGeneral(rutaSalida, urlRestApi);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return respuesta;
    }
    
    public boolean metodoGetGeneral(String rutaSalida, String urlRestApi){
        List<AbstractDTO> listaDTO = new ArrayList<>();
        final String NEXT_LINE = "\n";
        String delim =",";
        try {
            JsonParser parser = new JsonParser();
            // Obtain Array
            JsonArray gsonArr = parser.parse(metodoGet(urlRestApi)).getAsJsonArray();
            // for each element of array
            for (JsonElement obj : gsonArr) {
            // Object of array
            JsonObject gsonObj = obj.getAsJsonObject();
            
            ////**********ajustes****************
            //if(!gsonObj.get("name").isJsonNull() && !gsonObj.get("desc").isJsonNull() && !gsonObj.get("img").isJsonNull()){
                AbstractDTO inDtO = new AbstractDTO();
                inDtO = objectMapper.readValue(gsonObj.toString(), AbstractDTO.class);
                listaDTO.add(inDtO);
                //}
            ////**********ajustes****************
            }
            /////////////////////crear el documento
            FileWriter fw = new FileWriter(rutaSalida+".csv");
            ////**********ajustes****************
            String titulos = "	"+delim+"Country Code"+delim+"Indicator Name"+delim+"Indicator Code"+delim+"1960"+delim+"1961"+delim+"1962"+delim+"1963"+delim+"1964"+delim+"1965"+delim+"1966"+delim+"1967"+delim+"1968"+delim+"1969"+delim+"1970"+delim+"1971"+delim+"1972"+delim+"1973"+delim+"1974"+delim+"1975"+delim+"1976"+delim+"1977"+delim+"1978"+delim+"1979"+delim+"1980"+delim+"1981"+delim+"1982"+delim+"1983"+delim+"1984"+delim+"1985"+delim+"1986"+delim+"1987"+delim+"1988"+delim+"1989"+delim+"1990"+delim+"1991"+delim+"1992"+delim+"1993"+delim+"1994"+delim+"1995"+delim+"1996"+delim+"1997"+delim+"1998"+delim+"1999"+delim+"2000"+delim+"2001"+delim+"2002"+delim+"2003"+delim+"2004"+delim+"2005"+delim+"2006"+delim+"2007"+delim+"2008"+delim+"2009"+delim+"2010"+delim+"2011"+delim+"2012"+delim+"2013"+delim+"2014"+delim+"2015"+delim+"2016"+delim+"2017";
            fw.append(titulos);
            fw.append(NEXT_LINE);
            for(int i =0; i < listaDTO.size(); i++){
                fw.append(listaDTO.get(i).getCountry());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getCountryCode());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getIndicatorName());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getIndicatorCode());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1960());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1961());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1962());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1963());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1964());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1965());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1966());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1967());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1968());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1969());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1970());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1971());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1972());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1973());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1974());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1975());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1976());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1977());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1978());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1979());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1980());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1981());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1982());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1983());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1984());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1985());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1986());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1987());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1988());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1989());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1990());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1991());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1992());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1993());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1994());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1995());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1996());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1997());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1998());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA1999());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2000());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2001());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2002());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2003());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2004());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2005());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2006());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2007());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2008());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2009());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2010());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2011());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2012());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2013());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2014());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2015());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2016());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getA2017());
                  fw.append(NEXT_LINE);
            }
            ////**********ajustes****************
            fw.flush();
            fw.close();
            /////////////////////termina crear el documento
	} catch (MalformedURLException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
        return true;
    }
    
    public boolean metodoGetDeforestacion(String rutaSalida, String urlRestApi){
        Gson gson = new Gson();
        List<DeforestacionDTO> listaDTO = new ArrayList<>();
        final String NEXT_LINE = "\n";
        String delim =",";
        try {
            JsonParser parser = new JsonParser();
            // Obtain Array
            JsonArray gsonArr = parser.parse(metodoGet(urlRestApi)).getAsJsonArray();
            // for each element of array
            for (JsonElement obj : gsonArr) {
            // Object of array
            JsonObject gsonObj = obj.getAsJsonObject();
            ////**********ajustes****************
            //if(!gsonObj.get("name").isJsonNull() && !gsonObj.get("desc").isJsonNull() && !gsonObj.get("img").isJsonNull()){
                DeforestacionDTO inDTO = new DeforestacionDTO();
                inDTO = objectMapper.readValue(gsonObj.toString(), DeforestacionDTO.class);
                listaDTO.add(inDTO);
               // }
            ////**********ajustes****************
            }
            /////////////////////crear el documento
            FileWriter fw = new FileWriter(rutaSalida+".csv");
            ////**********ajustes****************
            String titulos = " "+delim+"iso3"+delim+"wdpa-id"+delim+"park name"+delim+"year"+delim+"outside"+delim+"inside";
            fw.append(titulos);
            fw.append(NEXT_LINE);
            for(int i =0; i < listaDTO.size(); i++){
                fw.append(listaDTO.get(i).getCountry());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getIso3());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getWdpaId());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getParkName());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getYear());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getOutside());
                  fw.append(delim);
                fw.append(listaDTO.get(i).getInside());
                  fw.append(NEXT_LINE);
            }
            ////**********ajustes****************
            fw.flush();
            fw.close();
            /////////////////////termina crear el documento
	} catch (MalformedURLException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
        return true;
    }
   
}
package com.mycompany.generararchivosdata;

import DTO.AbstractDTO;
import DTO.DeforestacionDTO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.map.ObjectMapper;

public class NetClientPost extends AbstractDelegate{

    ObjectMapper objectMapper = new ObjectMapper();
    private static final String EMPTY="";
        public boolean gestionPost(String opcion,String rutaSalida, String urlRestApi){
            boolean respuesta = false;
            try{
                if(opcion.equalsIgnoreCase("Deforestacion")){
                    respuesta = metodoPostGestionDeforestacion(rutaSalida, urlRestApi);
                }else{
                   respuesta = metodoPostGestionGeneral(rutaSalida, urlRestApi);     
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return respuesta;
        }
        public boolean metodoPostGestionGeneral(String rutaArchivo, String urlRestApi){
            try {
                        ////**********ajustes****************
                        
                        List<AbstractDTO> listaObjeto = loadFileGeneral(rutaArchivo);  //se obtiene la lista de objetos para cargar 
                        for (int i = 0; i < listaObjeto.size(); i++) {
                            metodoPost(listaObjeto.get(i), urlRestApi);  //realiza la carga
                         }
                        ////**********ajustes****************

		} catch (Exception e) {

			e.printStackTrace();

		}
            return true;
        }
        public boolean metodoPostGestionDeforestacion(String rutaArchivo, String urlRestApi){
            try {
                        ////**********ajustes****************
                        List<String> nombreCampos = new ArrayList<>();
                        nombreCampos.add("name");
                        nombreCampos.add("desc");
                        nombreCampos.add("img");
                        
                        List<DeforestacionDTO> listaObjeto = loadFileDeforestacion(rutaArchivo);  //se obtiene la lista de objetos para cargar 
                        for (int i = 0; i < listaObjeto.size(); i++) {
                            metodoPost(listaObjeto.get(i), urlRestApi);  //realiza la carga
                         }
                        ////**********ajustes****************

		} catch (Exception e) {

			e.printStackTrace();

		}
            return true;
        }
        
        public static List<AbstractDTO>  loadFileGeneral(String rutaArchivo) {
                ArrayList<AbstractDTO> valoresData = new ArrayList();    
		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))){ 
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0); // 0 Hoja1 -- 1 Hoja2 -- 2 Hoja3  --3 Hoja4
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
 
			Row row;
                        // se recorre cada fila hasta el final
			while (rowIterator.hasNext()) {
                            	row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada  celda
                                int conta = 0;
                                ArrayList<String> valores = new ArrayList();
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();
                                        valores.add(cell.toString());
                                        
				}
                                boolean flagProceso = false;
                                for(int j =0; j < valores.size(); j++){
                                    if(!EMPTY.equalsIgnoreCase(valores.get(j))){
                                        flagProceso = true;
                                    }
                                }
                                if(flagProceso){
                                    ////**********ajustes****************
                                    AbstractDTO objecto = new AbstractDTO();
                                    objecto.setCountry( (((valores.size() - 0) > 0 )?valores.get(0):"0").replace(",", "."));
                                    objecto.setCountryCode((((valores.size() - 1) > 0 )?valores.get(1):"0").replace(",", "."));
                                    objecto.setIndicatorName((((valores.size() - 2) > 0 )?valores.get(2):"0").replace(",", "."));
                                    objecto.setIndicatorCode((((valores.size() - 3) > 0 )?valores.get(3):"0").replace(",", "."));
                                    objecto.setA1960((((valores.size() - 4) > 0 )?valores.get(4):"0").replace(",", "."));
                                    objecto.setA1961((((valores.size() - 5) > 0 )?valores.get(5):"0").replace(",", "."));
                                    objecto.setA1962((((valores.size() - 6) > 0 )?valores.get(6):"0").replace(",", "."));
                                    objecto.setA1963((((valores.size() - 7) > 0 )?valores.get(7):"0").replace(",", "."));
                                    objecto.setA1964((((valores.size() - 8) > 0 )?valores.get(8):"0").replace(",", "."));
                                    objecto.setA1965((((valores.size() - 9) > 0 )?valores.get(9):"0").replace(",", "."));
                                    objecto.setA1966((((valores.size() - 10) > 0 )?valores.get(10):"0").replace(",", "."));
                                    objecto.setA1967((((valores.size() - 11) > 0 )?valores.get(11):"0").replace(",", "."));
                                    objecto.setA1968((((valores.size() - 12) > 0 )?valores.get(12):"0").replace(",", "."));
                                    objecto.setA1969((((valores.size() - 13) > 0 )?valores.get(13):"0").replace(",", "."));
                                    objecto.setA1970((((valores.size() - 14) > 0 )?valores.get(14):"0").replace(",", "."));
                                    objecto.setA1971((((valores.size() - 15) > 0 )?valores.get(15):"0").replace(",", "."));
                                    objecto.setA1972((((valores.size() - 16) > 0 )?valores.get(16):"0").replace(",", "."));
                                    objecto.setA1973((((valores.size() - 17) > 0 )?valores.get(17):"0").replace(",", "."));
                                    objecto.setA1974((((valores.size() - 18) > 0 )?valores.get(18):"0").replace(",", "."));
                                    objecto.setA1975((((valores.size() - 19) > 0 )?valores.get(19):"0").replace(",", "."));
                                    objecto.setA1976((((valores.size() - 20) > 0 )?valores.get(20):"0").replace(",", "."));
                                    objecto.setA1977((((valores.size() - 21) > 0 )?valores.get(21):"0").replace(",", "."));
                                    objecto.setA1978((((valores.size() - 22) > 0 )?valores.get(22):"0").replace(",", "."));
                                    objecto.setA1979((((valores.size() - 23) > 0 )?valores.get(23):"0").replace(",", "."));
                                    objecto.setA1980((((valores.size() - 24) > 0 )?valores.get(24):"0").replace(",", "."));
                                    objecto.setA1981((((valores.size() - 25) > 0 )?valores.get(25):"0").replace(",", "."));
                                    objecto.setA1982((((valores.size() - 26) > 0 )?valores.get(26):"0").replace(",", "."));
                                    objecto.setA1983((((valores.size() - 27) > 0 )?valores.get(27):"0").replace(",", "."));
                                    objecto.setA1984((((valores.size() - 28) > 0 )?valores.get(28):"0").replace(",", "."));
                                    objecto.setA1985((((valores.size() - 29) > 0 )?valores.get(29):"0").replace(",", "."));
                                    objecto.setA1986((((valores.size() - 30) > 0 )?valores.get(30):"0").replace(",", "."));
                                    objecto.setA1987((((valores.size() - 31) > 0 )?valores.get(31):"0").replace(",", "."));
                                    objecto.setA1988((((valores.size() - 32) > 0 )?valores.get(32):"0").replace(",", "."));
                                    objecto.setA1989((((valores.size() - 33) > 0 )?valores.get(33):"0").replace(",", "."));
                                    objecto.setA1990((((valores.size() - 34) > 0 )?valores.get(34):"0").replace(",", "."));
                                    objecto.setA1991((((valores.size() - 35) > 0 )?valores.get(35):"0").replace(",", "."));
                                    objecto.setA1992((((valores.size() - 36) > 0 )?valores.get(36):"0").replace(",", "."));
                                    objecto.setA1993((((valores.size() - 37) > 0 )?valores.get(37):"0").replace(",", "."));
                                    objecto.setA1994((((valores.size() - 38) > 0 )?valores.get(38):"0").replace(",", "."));
                                    objecto.setA1995((((valores.size() - 39) > 0 )?valores.get(39):"0").replace(",", "."));
                                    objecto.setA1996((((valores.size() - 40) > 0 )?valores.get(40):"0").replace(",", "."));
                                    objecto.setA1997((((valores.size() - 41) > 0 )?valores.get(41):"0").replace(",", "."));
                                    objecto.setA1998((((valores.size() - 42) > 0 )?valores.get(42):"0").replace(",", "."));
                                    objecto.setA1999((((valores.size() - 43) > 0 )?valores.get(43):"0").replace(",", "."));
                                    objecto.setA2000((((valores.size() - 44) > 0 )?valores.get(44):"0").replace(",", "."));
                                    objecto.setA2001((((valores.size() - 45) > 0 )?valores.get(45):"0").replace(",", "."));
                                    objecto.setA2002((((valores.size() - 46) > 0 )?valores.get(46):"0").replace(",", "."));
                                    objecto.setA2003((((valores.size() - 47) > 0 )?valores.get(47):"0").replace(",", "."));
                                    objecto.setA2004((((valores.size() - 48) > 0 )?valores.get(48):"0").replace(",", "."));
                                    objecto.setA2005((((valores.size() - 49) > 0 )?valores.get(49):"0").replace(",", "."));
                                    objecto.setA2006((((valores.size() - 50) > 0 )?valores.get(50):"0").replace(",", "."));
                                    objecto.setA2007((((valores.size() - 51) > 0 )?valores.get(51):"0").replace(",", "."));
                                    objecto.setA2008((((valores.size() - 52) > 0 )?valores.get(52):"0").replace(",", "."));
                                    objecto.setA2009((((valores.size() - 53) > 0 )?valores.get(53):"0").replace(",", "."));
                                    objecto.setA2010((((valores.size() - 54) > 0 )?valores.get(54):"0").replace(",", "."));
                                    objecto.setA2011((((valores.size() - 55) > 0 )?valores.get(55):"0").replace(",", "."));
                                    objecto.setA2012((((valores.size() - 56) > 0 )?valores.get(56):"0").replace(",", "."));
                                    objecto.setA2013((((valores.size() - 57) > 0 )?valores.get(57):"0").replace(",", "."));
                                    objecto.setA2014((((valores.size() - 58) > 0 )?valores.get(58):"0").replace(",", "."));
                                    objecto.setA2015((((valores.size() - 59) > 0 )?valores.get(59):"0").replace(",", "."));
                                    objecto.setA2016((((valores.size() - 60) > 0 )?valores.get(60):"0").replace(",", "."));
                                    objecto.setA2017((((valores.size() - 61) > 0 )?valores.get(61):"0").replace(",", "."));
                                    valoresData.add(objecto);
                                    ////**********ajustes****************
                                }
 			}
		} catch (Exception e) {
			e.printStackTrace();
		}
                return valoresData;
	}
        
        
        public static List<DeforestacionDTO>  loadFileDeforestacion(String rutaArchivo) {
                ArrayList<DeforestacionDTO> valoresData = new ArrayList();    
		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))){ 
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0); // 0 Hoja1 -- 1 Hoja2 -- 2 Hoja3  --3 Hoja4
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
 
			Row row;
                        // se recorre cada fila hasta el final
			while (rowIterator.hasNext()) {
                        	row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
                                int conta = 0;
                                ArrayList<String> valores = new ArrayList();
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();
                                            valores.add(cell.toString());
                                        
				}
                                boolean flagProceso = false;
                                for(int j =0; j < valores.size(); j++){
                                    if(!EMPTY.equalsIgnoreCase(valores.get(j))){
                                        flagProceso = true;
                                    }
                                }
                                if(flagProceso){
                                    ////**********ajustes****************
                                    DeforestacionDTO objecto = new DeforestacionDTO();
                                    objecto.setCountry( (((valores.size() - 0) > 0 )?valores.get(0):"0").replace(",", "."));
                                    objecto.setIso3((((valores.size() - 1) > 0 )?valores.get(1):"0").replace(",", "."));
                                    objecto.setWdpaId((((valores.size() - 2) > 0 )?valores.get(2):"0").replace(",", "."));
                                    objecto.setParkName((((valores.size() - 3) > 0 )?valores.get(3):"0").replace(",", "."));
                                    objecto.setYear((((valores.size() - 4) > 0 )?valores.get(4):"0").replace(",", "."));
                                    objecto.setOutside((((valores.size() - 5) > 0 )?valores.get(5):"0").replace(",", "."));
                                    objecto.setInside((((valores.size() - 6) > 0 )?valores.get(6):"0").replace(",", "."));
                                    valoresData.add(objecto);
                                    ////**********ajustes****************
                                }
                        }
		} catch (Exception e) {
			e.printStackTrace();
		}
                return valoresData;
	}

}
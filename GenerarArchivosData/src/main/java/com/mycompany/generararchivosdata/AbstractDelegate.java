/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.generararchivosdata;

import DTO.AreasSelvaticasDTO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Alex Vizcaino
 */
public class AbstractDelegate{
    
    ObjectMapper objectMapper = new ObjectMapper();
    
   public String getToken(String urlRestApi){
        try {
            URL url = new URL(urlRestApi);                
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            String respuestaJson ="";
            while ((output = br.readLine()) != null) {
                   respuestaJson = output; //datos del metodo get restapi
                   
            }
            JsonParser parser = new JsonParser();
            // Obtain objectJson
            JsonObject gsonArr = parser.parse(respuestaJson).getAsJsonObject();
            conn.disconnect();
            JsonElement dato =  gsonArr.get("token");
            return dato.getAsString();
            
	} catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
	} catch (IOException e) {
            e.printStackTrace();
            return null;
	}
    }
   
   public String metodoGet(String urlRestApi){
        try {
            String urlToken="";
            if(urlRestApi.contains("3000")){
                String [] url = urlRestApi.split("3000");
                urlToken = url[0]+"3000/token";
            }else{
                String [] url = urlRestApi.split("4000");
                urlToken = url[0]+"4000/token";
            }
            URL url = new URL(urlRestApi);                
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "autorizacion " + getToken(urlToken));
	    conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            String respuestaJson ="";
            while ((output = br.readLine()) != null) {
                respuestaJson = output; //datos del metodo get restapi
                   
            }
	    conn.disconnect();
            return respuestaJson;
	} catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
	} catch (IOException e) {
            e.printStackTrace();
            return null;
	}
    }

    public void metodoPost( Object objectDTO, String urlRestApi){
            try {
                        String urlToken="";
                        if(urlRestApi.contains("3000")){
                            String [] url = urlRestApi.split("3000");
                            urlToken = url[0]+"3000/token";
                        }else{
                            String [] url = urlRestApi.split("4000");
                            urlToken = url[0]+"4000/token";
                        }
			URL url = new URL(urlRestApi);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
                        conn.setRequestProperty("Authorization", "autorizacion " + getToken(urlToken));
			conn.setRequestProperty("Content-Type", "application/json");
                        ////**********ajustes****************
                        String input = objectMapper.writeValueAsString(objectDTO);
                        ////**********ajustes****************
                        OutputStream os = conn.getOutputStream();
                        os.write(input.getBytes());
                        os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {

				System.out.println(output);
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}
        }   
   
}